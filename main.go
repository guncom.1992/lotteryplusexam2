package main

import (
	"fmt"
	"strconv"
)

func convertCodeToNumString2(input string) string {
	var isFailed bool
	var rs, temp, digitCurrent string
	digit1 := 0

	for {
		rs, temp, digitCurrent = "", "", ""
		plus, tempPlus := 0, 0
		for i := 0; i < len(input); i++ {
			digitCurrent = fmt.Sprintf("%c", input[i])
			if temp == "" {
				temp, isFailed = conditionAction(digitCurrent, digit1, tempPlus)
				if !isFailed {
					break
				}
				rs += temp
			} else {
				iTemp, _ := strconv.Atoi(temp)
				temp, isFailed = conditionAction(digitCurrent, iTemp, tempPlus)
				if !isFailed {
					if i > 3 {
						if fmt.Sprintf("%c", input[i-3]) == "R" {
							i -= 3
							plus += 1
							tempPlus = plus
							temp = rs[len(rs)-3 : len(rs)-2]
							rs = rs[:len(rs)-2]
						}
					} else {
						break
					}
				} else {
					tempPlus = 0
					rs += temp
				}
			}
			if i == len(input)-1 {
				rs = fmt.Sprintf("%d%s", digit1, rs)
				break
			}
		}
		if len(rs) == 6 {
			break
		}
		digit1 += 1
	}
	return rs
}

func convertCodeToNumString(input string, digitStart int) string {
	var digitCurrent, tempResult, rs string
	var isFailed bool
	tempDigit := digitStart

	for i := 0; i < len(input); i++ {
		digitCurrent = fmt.Sprintf("%c", input[i])
		if i+3 <= len(input) {
			if input[i:i+3] == "RLL" {
				tempResult = processWithRLL(tempDigit)
				i += 3
			} else {
				tempResult, isFailed = conditionAction(digitCurrent, tempDigit, 0)
				tempDigit, _ = strconv.Atoi(tempResult)
				if isFailed {
					break
				}
			}
		} else {
			tempResult, isFailed = conditionAction(digitCurrent, tempDigit, 0)
			tempDigit, _ = strconv.Atoi(tempResult)
			if isFailed {
				break
			}
		}
		rs += tempResult

	}
	return rs
}

func processWithRLL(lastDigit int) string {
	var rs, tempResult string
	var plus int
	var isFailed bool
	code := []string{"R", "L", "L"}
	tempDigit := lastDigit

	for {
		rs = ""
		for i := range code {
			tempResult, isFailed = conditionAction(code[i], tempDigit, plus)
			if isFailed {
				break
			}
			tempDigit, _ = strconv.Atoi(tempResult)
			rs += tempResult
		}
		if len(rs) == 3 {
			break
		}
		plus += 1
	}
	return rs
}

func conditionAction(s string, n, plus int) (string, bool) {
	switch s {
	case "L":
		if n-1 < 0 {
			return "", true
		}
		return strconv.Itoa(n - 1), false
	case "R":
		return strconv.Itoa(n + 1 + plus), false
	case "=":
		return strconv.Itoa(n), false
	}
	return "", true
}
func main() {
	var rs string
	var digitStart int
	// input = LLRR= output = 210122 DONE
	// input = ==RLL output = 000210 DONE
	// input = =LLRR output = 221012 DONE
	input := "=LLRR"

	for {
		rs = convertCodeToNumString(input, digitStart)
		if len(rs) == len(input) {
			break
		}
		digitStart += 1
	}
	rs = fmt.Sprintf("%d%s", digitStart, rs)
	fmt.Println(rs)
}
